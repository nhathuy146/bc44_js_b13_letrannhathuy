// Bài 1
function tinhTien() {
  // Input
  const luongMotNgay = 100000;
  var soNgayLam = document.getElementById("so-ngay-lam").value * 1;
  //   Xử lý
  var luongMotThang = luongMotNgay * soNgayLam;
  luongMotThang = new Intl.NumberFormat("vn-VN").format(luongMotThang);
  //   Output
  document.getElementById("result").innerText = `${luongMotThang} VND`;
}

// Bài 2
function tinhGiaTriTB() {
  // input
  var num1 = document.getElementById("num-1").value * 1;
  var num2 = document.getElementById("num-2").value * 1;
  var num3 = document.getElementById("num-3").value * 1;
  var num4 = document.getElementById("num-4").value * 1;
  var num5 = document.getElementById("num-5").value * 1;
  //   Xử lý
  var giaTriTB = (num1 + num2 + num3 + num4 + num5) / 5;
  // Output
  document.getElementById("result_2").innerHTML = `${giaTriTB}`;
}

// Bài 3
function doiTien() {
  // Input
  const vnd = 23500;
  var tienUSD = document.getElementById("tien-usd").value * 1;
  // Xử lý
  var tienViet = vnd * tienUSD;
  tienViet = new Intl.NumberFormat("vn-VN").format(tienViet);
  // Output
  document.getElementById("result_3").innerText = `${tienViet} VND`;
}

// Bài 4
function tinhHCN() {
  // Input
  var chieuDai = document.getElementById("chieu-dai").value * 1;
  var chieuRong = document.getElementById("chieu-rong").value * 1;
  // Xử Lý
  var dienTich = chieuDai * chieuRong;
  var chuVi = (chieuDai + chieuRong) * 2;
  // Output
  document.getElementById("result_4_1").innerText = `${dienTich}`;
  document.getElementById("result_4_2").innerText = `${chuVi}`;
}

// Bài 5
function tinhTong() {
  // input
  var laySo = document.getElementById("ky-so").value * 1;
  // Xử lý
  var layHangChuc = Math.floor(laySo / 10);
  var layHangDonVi = laySo % 10;
  var tongHaiKySo = layHangChuc + layHangDonVi;
  // Output
  document.getElementById("result_5").innerText = `${tongHaiKySo}`;
}
